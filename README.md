Amen is a Christian approach to mindfulness. We are creating a new, digital way to connect with Jesus Christ as a daily escape from the busy world around us.

We offer the word of the Lord in form of Bible stories, Christian meditations, daily prayer, and more, as a way to reduce anxiety, promote better sleep, manage stress and help bring joy to everyday life.

Amen is calmness through Christ.

Website: https://amen.io/
